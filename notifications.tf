# Attaching a notification to bucket and topic

resource "google_storage_notification" "bucket_notification" {
  bucket = var.bucket
  topic = var.topic
  event_types = ["OBJECT_FINALIZE"]
  payload_format = "NONE"
  depends_on = var.depends_on
}
